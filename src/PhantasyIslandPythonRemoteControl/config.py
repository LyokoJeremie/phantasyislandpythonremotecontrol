"""
此文件编码了仿真平台的远程控制端口
"""
remote_location = '127.0.0.1:12566'

http_retry_times = 10
http_timeout_cmd_connect = 1
http_timeout_cmd_read = 10
